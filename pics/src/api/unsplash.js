import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 'Client-ID hli6AOSiiA4oXRhnlCBFaowFVtMNOnQ1iBv2vnBKStk',
  }
});
