import React from 'react';
import { connect } from 'react-redux';

const SongDetail = ({ song }) => {
  if (!song) {
    return (
      <div>
        <h3>Please select a song</h3>
      </div>
    );
  }

  return (
    <div className="ui fluid card">
      <div className="content">
        <div className="header">{song.title}</div>
        {/* <div className="meta">{song.band}</div> */}
        <div className="description">{song.band}</div>
      </div>

      {/* <div className="extra conten"> */}
      {/*   <div className="ui labeled button" tabIndex="0"> */}
      {/*     <div className="ui button"> <i className="heart icon"></i> Like </div> */}
      {/*     <i className="ui basic label"> 2,048 </i> */}
      {/*   </div> */}
      {/*   <div className="ui labeled button" tabIndex="0"> */}
      {/*     <div className="ui red button"> <i className="heart icon"></i> Like </div> */}
      {/*     <i className="ui basic red left pointing label"> 1,048 </i> */}
      {/*   </div> */}
      {/* </div> */}

    </div>
  );
};


const mapStateToProps = (state) => {
  return {
    song: state.selectedSong,
  };
};


export default connect(mapStateToProps)(SongDetail);
