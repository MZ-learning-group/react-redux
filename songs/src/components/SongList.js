import React from 'react';
import { connect } from 'react-redux';
import { selectSong } from '../actions';


const SongList = ({ songs, selectedSongTitle, selectSong }) => {

  const renderList = songs.map(song => {
    return (
      <div key={song.title} className="item">
        <div className="right floated content">
          <div
            className={`ui button ${selectedSongTitle === song.title ? 'disabled' : ''}`}
            onClick={() => selectSong(song)}
          >Select</div>
        </div>
        <div className="content">
          <div className="header">{song.title}</div>
          {song.band}
        </div>
      </div>
    );
  });

  return (
    <div className="ui celled list">
      {renderList}
    </div>
  );
};


const mapStateToProps = (state) => {
  return {
    songs: state.songs,
    selectedSongTitle: state.selectedSong?.title,
  };
};

export default connect(mapStateToProps, { selectSong })(SongList);
