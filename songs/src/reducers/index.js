import { combineReducers } from 'redux';


const songsReducer = () => {
  return [
    {
      title: 'Smells Like Teen Spirit',
      band: 'Nirvana',
    },
    {
      title: 'Baby Got Back',
      band: 'Sir Mix-a-Lot',
    },
    {
      title: 'Wannabe',
      band: 'Spice Girls',
    },
    {
      title: 'Doo Wop(That Thing)',
      band: 'Lauryn Hill',
    },
    {
      title: 'Creep',
      band: 'Radiohead',
    },

  ];
};


const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === 'SONG_SELECTED') {
    return action.payload;
  }

  return selectedSong;
}


export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer,
});
