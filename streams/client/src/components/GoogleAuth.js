import React from 'react';
import { connect } from 'react-redux';
import { signIn, signOut } from '../actions';

class GoogleAuth extends React.Component {
  componentDidMount() {
    window.gapi.load('client:auth2', () => {
      window.gapi.client.init({
        client_id: '742371762208-nleo42pmiad885jeksn9484m6moe6519.apps.googleusercontent.com',
        scope: 'email',
      }).then(() => {
        this.auth = window.gapi.auth2.getAuthInstance();

        this.onAuthChange(this.auth.isSignedIn.get())
        this.auth.isSignedIn.listen(this.onAuthChange);
      });
    });
  }

  onAuthChange = (isSignedIn) => {
    if (isSignedIn) {
      this.props.signIn(this.auth.currentUser.get().getId());
    } else {
      this.props.signOut();
    }
  }

  onSignOutClick = () => {
    this.auth.signOut();
  }

  onSignInClick = () => {
    this.auth.signIn();
  }

  renderAuthButton() {
    if (this.props.isSignedIn === false) {
      return (
        <button className="ui button red google" onClick={this.onSignInClick}>
          <i className="google icon" />
          Sign In with google
        </button>
      );
    }
    if (this.props.isSignedIn === true) {
      return (
        <button className="ui button red google" onClick={this.onSignOutClick}>
          <i className="google icon" />
          Sign Out
        </button>
      );
    }
    return <button className="ui loading button red">Loading</button>;
  }

  render() {
    return <div className="item">{this.renderAuthButton()}</div>;
  }
}


const mapStateToProps = (state) => {
  return { isSignedIn: state.auth.isSignedIn }
}

export default connect(mapStateToProps, { signIn, signOut })(GoogleAuth);
