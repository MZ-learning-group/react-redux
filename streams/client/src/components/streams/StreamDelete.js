import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deleteStream, fetchStream } from '../../actions';
import Modal from '../Modal';


const StreamDelete = ({ stream, deleteStream, fetchStream, match, history }) => {
  useEffect(() => {
    if (!stream) {
      fetchStream(match.params.id);
    }
  }, [fetchStream, match.params.id]);

  const onDeleteClick = (e) => {
    e.target.classList.add('loading', 'disabled')
    deleteStream(match.params.id);
  };

  const actions = (
    <>
      <div className="ui button negative" onClick={onDeleteClick}>Delete</div>
      <Link to="/" className="ui button">Cancel</Link>
    </>
  );

  const renderContent = () => {
    if (!stream) {
      return (
        <div>
          <div className="ui active inverted dimmer">
            <div className="ui text loader">Loading</div>
          </div>
        <p>
          Are you sure you want to delete the stream?
        </p>
        </div>
      );
    }
    return (
      <p>{`Are you sure you want to delete ${stream.title} stream?`}</p>
    );
  }

  return (
    <Modal
      actions={actions}
      onDismiss={() => history.push('/')}
      title="Delete Stream" >
      {renderContent()}
    </Modal>
  );

};


const mapStateToProps = (state, ownProps) => {
  return { stream: state.streams[ownProps.match.params.id] }
}

export default connect(mapStateToProps, { deleteStream, fetchStream })(StreamDelete);
