import _ from 'lodash';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { fetchStream, editStream } from '../../actions';
import StreamForm from './StreamForm';


const StreamEdit = ({ stream, fetchStream, editStream, match }) => {
  useEffect(() => {
    if (!stream) {
      fetchStream(match.params.id);
    }
  }, [fetchStream, match.params.id]);


  const onSubmit = (formValues) => {
    editStream(stream.id, formValues);
  }

  if (!stream) {
    return <div className="ui active loader"></div>
  }

  return (
    <div>
      <h2>Edit a Stream</h2>
      <StreamForm onSubmit={onSubmit} initialValues={_.pick(stream, 'title', 'description')} />
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id],
  };

}
export default connect(mapStateToProps, { fetchStream, editStream })(StreamEdit);
