import React, { useEffect, useRef } from 'react';
import flv from 'flv.js';
import { connect } from 'react-redux';
import { fetchStream } from '../../actions';


const StreamShow = ({ stream, match, fetchStream }) => {
  const videoRef = useRef();
  useEffect(() => {
    if (!stream) {
      fetchStream(match.params.id);
    }
    else {
      if (flv.isSupported()) {
        var flvPlayer = flv.createPlayer({
          type: 'flv',
          url: `http://localhost:8000/live/${stream.id}.flv`
        });
        flvPlayer.attachMediaElement(videoRef.current);
        flvPlayer.load();
        return () => { flvPlayer.destroy() }
      }
    }
  }, [fetchStream, stream, match.params.id]);

  if (!stream) {
    return <div className="ui active loader"></div>;
  }

  return (
    <div>
      <video ref={videoRef} style={{ width: '100%' }} controls />
      <h1>{stream.title}</h1>
      <h5>{stream.description}</h5>
    </div >
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    stream: state.streams[ownProps.match.params.id],
  };

}
export default connect(mapStateToProps, { fetchStream })(StreamShow);
