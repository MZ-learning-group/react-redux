import axios from 'axios';

const KEY = 'AIzaSyAXmDRp3n45_aWWfPspJyRl9VMu95IDPLA';


export default axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params: {
    part: 'snippet',
    type: 'video',
    maxResults: 5,
    key: KEY,
  }
})
