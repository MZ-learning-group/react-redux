import React from 'react';
import Accordion from './components/Accordion';
import Search from './components/Search';
import Dropdown from './components/Dropdown';
import Translate from './components/Translate';
import Header from './components/Header';
import Route from './components/Route';

const items = [
  {
    title: 'What is React?',
    content: 'React is a front end javascript framework',
  },
  {
    title: 'Why use React?',
    content: 'React is a favorite JS library among engineers',
  },
  {
    title: 'How da you use React?',
    content: 'You use React by creating components',
  },
];

const options = [
  {
    label: 'The Color Red',
    value: 'red',
  },
  {
    label: 'The Color Green',
    value: 'green',
  },
  {
    label: 'The Color Blue',
    value: 'blue',
  },
];


const App = () => {
  const [selected, setSelected] = React.useState(options[0])

  return (
    <div className="ui container">
      <Header />

      <Route path="/">
        <Accordion items={items} />
      </Route>

      <Route path="/translate">
        <Translate />
      </Route>

      <Route path="/dropdown">
        <Dropdown options={options} selected={selected} onSelectedChange={setSelected} title="Select a Collor" />
      </Route>

      <Route path="/search">
        <Search />
      </Route>


    </div>
  );
};


export default App;
