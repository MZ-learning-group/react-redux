import React, { useEffect, useState } from 'react';
import axios from 'axios';


const Convert = ({ language, text }) => {
  const [translated, setTranslated] = useState('');
  const [debouncedText, setDebouncedText] = useState('');

  useEffect(() => {
    if (debouncedText) {
      (async () => {
        const res = await axios.post(
          'https://translation.googleapis.com/language/translate/v2',
          {},
          { params: { q: debouncedText, target: language.value, key: 'AIzaSyCHUCmpR7cT_yDFHC98CZJy2LTms-IwDlM', } }
        );

        setTranslated(res.data.data.translations[0].translatedText)
      })();
    }
  }, [debouncedText, language.value]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setDebouncedText(text);
    }, 600);

    return () => {
      clearTimeout(timeout);
    }

  }, [language, text]);

  return (
    <div>
      <h1 className="ui header">{translated}</h1>
    </div>
  );

}

export default Convert;
