import React from 'react';


const Dropdown = ({ options, selected, onSelectedChange, title }) => {
  const [open, setOpen] = React.useState(false);
  const ref = React.useRef();

  React.useEffect(() => {
    const onBodyClick = event => {
      if (ref.current.contains(event.target)) {
        return;
      }
      setOpen(false);
    };

    document.body.addEventListener('click', onBodyClick, { capture: true });
    return () => document.body.removeEventListener('click', onBodyClick, { capture: true });
  }, []);


  const renderdOptions = options.map((option) => {
    if (option === selected) {
      return null;
    }
    return (
      <div key={option.value} className="item" onClick={() => onSelectedChange(option)}>
        {option.label}
      </div>
    );
  });

  return (
    <div className="ui form">
      <div className="field">
        <label className="label">{title}</label>

        <div
          className={`ui fluid search selection dropdown ${open ? 'visible active' : ''}`}
          onClick={() => setOpen(!open)}
          ref={ref}
        >
          <i className="dropdown icon"></i>
          <div className="text">{selected.label}</div>
          <div className={`menu ${open ? 'visible transition' : ''}`}>
            {renderdOptions}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dropdown;
