import React from 'react';

const Link = ({ href, className, children }) => {
  const onClick = (event) => {
    if (event.metaKey || event.ctrlKey) {
      return;
    }

    event.preventDefault();
    window.history.pushState({}, '', href);

    const naveEvent = new PopStateEvent('popstate');
    window.dispatchEvent(naveEvent);
  }
  return (
    <a onClick={onClick} href={href} className={className}>
      {children}
    </a>
  );
}

export default Link;
