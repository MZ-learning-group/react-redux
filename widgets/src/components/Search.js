import React from 'react';
import axios from 'axios';
import './Search.css';


const Search = () => {
  const [term, setTerm] = React.useState('React.JS');
  const [debouncedTerm, setDebouncedTerm] = React.useState(term);
  const [results, setResults] = React.useState([]);

  React.useEffect(() => {
    (async () => {
      const response = await axios.get(
        'https://en.wikipedia.org/w/api.php',
        {
          params: {
            action: 'query',
            list: 'search',
            origin: '*',
            format: 'json',
            srsearch: debouncedTerm,
          }
        }
      );
      setResults(response.data.query.search);
    })();
  }, [debouncedTerm]);

  React.useEffect(() => {
    const timeoutId = setTimeout(() => {
      if (term) setDebouncedTerm(term)
    }, 400);
    return () => {
      clearTimeout(timeoutId);
    }
  }, [term]);

  const renderdResults = results.map(({ snippet, title, pageid }) => {
    return (
      <div key={pageid} className="item">
        <div className="content">
          <div className="right floated content">
            <a className="ui button" href={`https://en.wikipedia.org?curid=${pageid}`}>
              Go
            </a>

          </div>
          <div className="header">{title}</div>
          <span dangerouslySetInnerHTML={{ __html: snippet }}></span>
        </div>
      </div>
    );
  });

  return (
    <div>
      <div className="ui form">
        <div className="filed">
          <label>Enter Search Term</label>
          <input className="input" type="text" value={term} onChange={e => setTerm(e.target.value)} />
        </div>
      </div>

      <div className="ui celled list">
        {renderdResults}
      </div>
    </div>
  );
};

export default Search;
