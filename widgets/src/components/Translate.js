import React from 'react';
import Dropdown from './Dropdown';
import Convert from './Convert';


const options = [
  {
    label: 'Africaans',
    value: 'af',
  },
  {
    label: 'Arabic',
    value: 'ar',
  },
  {
    label: 'Hindi',
    value: 'hi',
  },
  {
    label: 'Dutch',
    value: 'nl',
  },
];


const Translate = () => {
  const [language, setLanguage] = React.useState(options[1]);
  const [text, setText] = React.useState('');

  return (
    <div>
      <div className="ui form">
        <div className="field">
          <label htmlFor="">Enter Text</label>
          <input type="text" value={text} onChange={e => setText(e.target.value)} />
        </div>
      </div>
      <Dropdown options={options} selected={language} onSelectedChange={setLanguage} title="Select a Language" />
      <h3 className="header">Output</h3>
      <Convert language={language} text={text} />
    </div>
  );
}
export default Translate;
